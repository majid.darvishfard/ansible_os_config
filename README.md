### Description
 This ansible playbook include 3 Role for install and configure **Docker daemon** , **Nginx service** and  **Firewalld Rule**.



### Docker 
 [README.md](https://gitlab.com/majid.darvishfard/ansible_os_config/-/tree/master/docker.install)


### Nginx
 [README.md](https://gitlab.com/majid.darvishfard/ansible_os_config/-/tree/master/nginx.install)


### Firewall rule 
 [README.md](https://gitlab.com/majid.darvishfard/ansible_os_config/-/tree/master/firewalld.config)




